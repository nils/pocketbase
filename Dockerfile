FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code 

ARG VERSION=0.22.12
RUN curl -L https://github.com/pocketbase/pocketbase/releases/download/v${VERSION}/pocketbase_${VERSION}_linux_amd64.zip -o pocketbase.zip \
    && unzip pocketbase.zip -d /app/code \
    && rm pocketbase.zip

RUN mkdir -p /app/data/pb_data
RUN mkdir -p  /app/data/pb_migrations
RUN mkdir -p  /app/data/pb_hooks 
RUN mkdir -p  /app/data/pb_public  
RUN touch /app/data/pb_public/index.html

CMD ["/app/code/pocketbase", "serve", "--http", "0.0.0.0:8090", "--dir", "/app/data/pb_data", "--hooksDir", "/app/data/pb_hooks", "--publicDir", "/app/data/pb_public", "--migrationsDir", "/app/data/pb_migrations"]

EXPOSE 8090