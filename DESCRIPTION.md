## About

PocketBase is an open source backend consisting of embedded database (SQLite) with realtime subscriptions, built-in auth management, convenient dashboard UI and simple REST-ish API.

## Features

### Realtime database
Embedded performant database with schema builder, data validations, realtime subscriptions and easy to use REST api.

### Authentication
Manage your app users and handle email/password and OAuth2 sign ups (Google, Facebook, GitHub, GitLab) without the hassle.

### File storage
Sanely store files locally or in a S3 storage. Easily attach media to your database records and generate thumbs on the fly.

### Extendable
Use as a standalone app OR as a framework, that you can extend via Go and JavaScript hooks to create your own custom portable backend.